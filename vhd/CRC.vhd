library ieee;
use ieee.std_logic_1164.all;

entity crc is
	port( clk: in std_logic;
		reset, clk_en: in bit;
		data: in std_logic;
		crc_out: out std_logic
		);
end;

architecture crc_arch of crc is
	type proc_states is (input, crc_first, crc_other, output);		-- input: for receiving data bit-by-bit & save it in a register
																-- crc_first: for the first division
																-- crc_other: for divisions after the first one
																-- output: sending "data & FCS" bit-by-bit to bit-to-symbol module
	signal current_st: proc_states:= input;
	signal crc_in: std_logic_vector(0 to 71):= (others=> '0');		-- string of 72 bits of data
	signal crc_data: std_logic_vector(0 to 87);						-- stores "data & FCS"
	-- constant zero: std_logic_vector(o to 16):= (others=> '0');
	constant poly: std_logic_vector(0 to 16):= "10001000000100001";			-- polynominal:		X^16+ X^12+ X^5+ 1
begin	
	process (clk, reset)
		variable cnt_in, cnt_crc: integer range 0 to 72;			-- counter for control the number of inputs & divisions
		variable cnt_out: integer range 0 to 88;					-- counter to control output
		variable remain, temp: std_logic_vector(0 to 16);
		variable input_var: std_logic_vector(0 to 71):= (others=> '0');
		variable crc_comp: std_logic_vector(0 to 87):= (others=> '0');
		
		begin
			if reset= '0' then 
				crc_out<= 'Z';
				current_st<= input;
			elsif current_st= input then
				if falling_edge(clk) then
					if clk_en= '0' then
						cnt_in:= cnt_in+ 1;
						input_var(71):= data;
						if cnt_in< 72 then
							for i in 0 to 70 loop input_var(i):= input_var(i+1); end loop;
						end if;
						if cnt_in= 72 then
							current_st<= crc_first;
							crc_in<= input_var;
							cnt_in:= 0;
						end if;
					end if;
				end if;
			elsif current_st= crc_first then
				if falling_edge(clk) then
					if clk_en= '0' then
						crc_comp(0 to 71):= crc_in;
						temp(0 to 16):= crc_comp(0 to 16);
						for i in 0 to 70 loop crc_comp(i):= crc_comp(i+17); end loop;
						if temp(0)= '0' then
							for i in 0 to 15 loop temp(i):= temp(i+1); end loop;
							temp(16):= crc_comp(0);
							for i in 0 to 86 loop crc_comp(i):= crc_comp(i+1); end loop;
							cnt_crc:= cnt_crc+ 1;
							current_st<= crc_other;
						elsif temp(0)= '1' then
							remain:= temp XOR poly;
							temp:= remain;
							for i in 0 to 15 loop temp(i):= temp(i+1); end loop;
							temp(16):= crc_comp(0);
							for i in 0 to 86 loop crc_comp(i):= crc_comp(i+1); end loop;
							cnt_crc:= cnt_crc+ 1;
							current_st<= crc_other;
						end if;
					end if;
				end if;
			elsif current_st= crc_other then
				if falling_edge(clk) then
					if clk_en= '0' then
						if cnt_crc< 72 then
							if temp(0)= '0' then
								for i in 0 to 15 loop
									temp(i):= temp(i+1);
									remain(i):= remain(i+1);
								end loop;
								cnt_crc:= cnt_crc+ 1;
							elsif temp(0)= '1' then
								remain:= temp XOR poly;
								temp:= remain;
								for i in 0 to 15 loop temp(i):= temp(i+1); end loop;
								cnt_crc:= cnt_crc+ 1;
							end if;
							if cnt_crc= 72 then
								crc_data<= crc_in & remain(1 to 16);
								current_st<= output;
								cnt_crc:= 0;
							end if;
						end if;
					end if;
				end if;
			elsif current_st= output then
				if falling_edge(clk) then
					if clk_en='0' then
						crc_out<= crc_data(0);
						for i in 0 to 86 loop crc_data(i)<= crc_data(i+1); end loop;
						cnt_out:= cnt_out +1;
						if cnt_out= 88 then
							current_st<= input;
							cnt_out:= 0;
						end if;
					end if;
				end if;
			end if;
	end process;
end;