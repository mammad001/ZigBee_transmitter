-- Connecting components to each other --
library ieee;
use ieee.std_logic_1164.all;

entity transmitter is
	port( data_in: in std_logic;
		  clk:in std_logic;
		  clk_en, reset: in bit;
		  i_chip, q_chip: out std_logic);
end transmitter;

architecture tx_arch of transmitter is
	-- Signals --
	signal div2, div8, div32: std_logic;
	signal crc_dout: std_logic;				-- data coming out of crc entering into bit-to-symbol component --
	signal symbol: std_logic_vector(0 to 3);
	signal sym_ready: bit;
	-- Components --
	component crc
	   port( clk: in std_logic;
		reset, clk_en: in bit;
		data: in std_logic;
		crc_out: out std_logic
		);
	end component;

	component clk_div
		port ( clk: in std_logic;
		reset, clk_en: in bit;
		clk_div2, clk_div8, clk_div32: out std_logic
		);
	end component;
		
	component bit_symbol_2
		port(din: in std_logic;
		clk: in std_logic;
		clk_en,reset: in bit;
		sym_out: out std_logic_vector(0 to 3);
		ready: out bit
		);
	end component;
	
	component sym_chip2
		port (sym_in: in std_logic_vector (0 to 3);
		clk_en, reset, ready: in bit;
		clk, clk_div8: in std_logic;
		i_chip, q_chip: out std_logic
		);
	end component;
	
	begin
	crc16: crc port map (div8, reset, clk_en, data_in, crc_dout);
	clock: clk_div port map (clk, reset, clk_en, div2, div8, div32);
	B_S: bit_symbol_2 port map (crc_dout, div8,clk_en, reset, symbol, sym_ready);
	S_CH: sym_chip2 port map (symbol, clk_en, reset, sym_ready, clk, div8, i_chip, q_chip);
end;