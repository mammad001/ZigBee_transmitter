-- Test Bench for CRC module--
library ieee;
use ieee.std_logic_1164.all;
entity tb_crc is
end;

architecture tb_crc_arch of tb_crc is
	signal data: std_logic;
	signal clk: std_logic;
	signal reset, clk_en: bit;
	signal crc_out: std_logic;
	component crc
		port( clk: in std_logic;
			  reset, clk_en: in bit;
			  data: in std_logic;
			  crc_out: out std_logic);
	end component;
	begin
	CRC1: crc port map(clk, reset, clk_en, data, crc_out);
	reset<= '0',
			'1' after 6 us;
	clk_en<= '1',
			 '0' after 9 us;
	clock: process
				variable cp: std_logic:= '0';
				begin
					cp:= NOT cp;
					clk<= cp;
					wait for 2 us;
		   end process;
	data_proc:	process
					variable d_in: std_logic_vector(0 to 73):= "10011011111111010001011101110011111000000011011000100110100001010010000011";	-- generated randomly via www.random.org/bytes
					begin
					data<= d_in(0);
					for i in 0 to 72 loop d_in(i):= d_in(i+1); end loop;
					wait for 4 us;
				end process;
end;