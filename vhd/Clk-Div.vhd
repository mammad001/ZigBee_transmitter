-- Clock Divider: div2, div 8 & div32 --
library ieee;
use ieee.std_logic_1164.all;
entity clk_div is
	port ( clk: in std_logic;
		reset, clk_en: in bit;
		clk_div2, clk_div8, clk_div32: out std_logic
		);
end clk_div;

architecture clk_div_arch of clk_div is
	signal cnt2: integer range 0 to 1 := 0;
	signal cnt8: integer range 0 to 7 := 0;
	signal cnt32: integer range 0 to 31 := 0;
	begin
-- ADD Process (reset) ------------------------------------------------------------------------------------------------------
	div2:	process (clk)
				begin
					if falling_edge(clk) then
						if reset= '0' then 
							clk_div2<= '0';
						elsif clk_en= '0' then
							if cnt2= 0 then 
								clk_div2<= '0';
								cnt2<= 1;
							else
								clk_div2<= '1';
								cnt2<= 0;
							end if;
					 end if;
					end if;
			end process div2;
	
	div8:	process (clk)
				begin
					if falling_edge(clk) then
						if reset= '0' then
							clk_div8<= '0';
						elsif clk_en= '0' then
							if cnt8<= 3 then
								clk_div8<= '0';
								cnt8<= cnt8+ 1;
							elsif (cnt8> 3 and cnt8< 7) then
								clk_div8<= '1';
								cnt8<= cnt8+ 1;
							elsif cnt8= 7 then
								cnt8<= 0;
							end if;
				  	end if;
				  end if;
			end process div8;
		
	div32:	process(clk)
				begin
					if falling_edge(clk) then
						if reset= '0' then
							clk_div32<= '0';
						elsif clk_en= '0' then
							if cnt32<= 15 then
								clk_div32<= '0';
								cnt32<= cnt32 + 1;
							elsif (cnt32> 15 and cnt32< 31) then
								clk_div32<= '1';
								cnt32<= cnt32 + 1;
							elsif cnt32= 31 then
								cnt32<= 0;
							end if;
						end if;
					end if;
				end process div32;
end;