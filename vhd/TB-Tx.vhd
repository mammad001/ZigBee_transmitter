-- Test Bench For The whole Transmitter
library ieee;
use ieee.std_logic_1164.all;

entity tb_tx is
end;

architecture tb_tx_arch of tb_tx is
	signal data_in: std_logic;
	signal clk: std_logic;
	signal clk_en, reset: bit;
	signal i_chip, q_chip: std_logic;
	component transmitter
		port( data_in: in std_logic;
			  clk:in std_logic;
		      clk_en, reset: in bit;
		      i_chip, q_chip: out std_logic);
	end component;
	begin
		tx: transmitter port map (data_in, clk, clk_en, reset, i_chip, q_chip);
		reset<= '0',
			'1' after 6 us;
		clk_en<= '1',
			 '0' after 9 us;
		clock: process
					variable cp: std_logic:= '0';
					begin
						cp:= NOT cp;
						clk<= cp;
						wait for 250 ns;
			   end process;
		data_proc:	process
						variable d_in: std_logic_vector(0 to 73):= "10011011111111010001011101110011111000000011011000100110100001010010000011";	-- generated randomly via www.random.org/bytes
						begin
						data_in<= d_in(0);
						for i in 0 to 72 loop d_in(i):= d_in(i+1); end loop;
						wait for 4 us;
					end process;
end;