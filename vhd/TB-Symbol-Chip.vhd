-- Test Bench for symbol-to-chip module --
library ieee;
use ieee.std_logic_1164.all;

entity tb_sym_chip is
end;

architecture tb_sym_chip_arch of tb_sym_chip is
  -- Signals --
	signal	sym_in: std_logic_vector (0 to 3);
	signal	clk_en, reset, ready: bit;
	signal	clk, clk_div8: std_logic;				-- clk:2 MHz for output		clk_div8: 250 KHz for input
	signal	i_chip, q_chip: std_logic;
	-- Component --
	component sym_chip2
		port (sym_in: in std_logic_vector (0 to 3);
		clk_en, reset, ready: in bit;
		clk, clk_div8: in std_logic;				-- clk:2 MHz for output		clk_div8: 250 KHz for input
		i_chip, q_chip: out std_logic
		);
	end component;
	begin
S2CH: sym_chip2	port map (sym_in, clk_en, reset, ready, clk, clk_div8, i_chip, q_chip);
		reset<= '0',
			     	'1' after 6 us;
		clk_en<= '1',
	       			 '0' after 9 us;
		ready<= '0',
			     	'1' after 24 us,
		      		'0' after 28 us,
		      		'1' after 42 us;
		sym_in<= "1110",
				     "0101" after 42 us;
	clock:  	process
				    variable cp: std_logic := '1';
				    begin
					   cp:= NOT cp;
				     clk<= cp;
		  	      wait for 250 ns;		-- half a cycle for a signal with frequency of 2 MHz --
			     end process;
	clock8:	process
				    variable cp8: std_logic := '1';
				    begin
				    	cp8:= NOT cp8;
					   clk_div8<= cp8;
		      			wait for 2 us;		-- half a cycle for a signal with frequency of 250 KHz --
			     end process;
end;