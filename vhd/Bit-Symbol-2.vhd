-- Bit to Symbol using enumrated type --
library ieee;
use ieee.std_logic_1164.all;

entity bit_symbol_2 is
	port(din: in std_logic;
		clk: in std_logic;
		clk_en,reset: in bit;
		sym_out: out std_logic_vector(0 to 3);
		ready: out bit);
end bit_symbol_2;

architecture bs2_arch of bit_symbol_2 is
	type state_type is (working, idle);
	signal current_st: state_type:= idle;
	begin
  	process (clk, reset)
  	  variable temp_value: std_logic_vector (0 to 3);
		 variable cnt: integer range 0 to 3:= 0;
			begin
				if reset= '0' then
					current_st<= idle;
					cnt:= 0;
					sym_out<= "0000";
				elsif falling_edge(clk) then
					if current_st= idle then
						ready<= '0';
						cnt:= 0;
						if clk_en= '0' then
							current_st<= working;
							temp_value(3):= din;
						end if;
					elsif current_st= working then
						if cnt< 3 then 
							for i in 0 to 2 loop temp_value(i):= temp_value(i+1); end loop;
							temp_value(3):= din;
							cnt:= cnt + 1;
						end if;
						if cnt= 3 then
						     sym_out<= temp_value;
						     ready<= '1';
							   current_st<= idle; 
						end if;
				end if;
			end if;
		end process;
end;