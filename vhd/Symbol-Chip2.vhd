-- Symbol To Chip --
library ieee;
use ieee.std_logic_1164.all;

entity sym_chip2 is
	port (sym_in: in std_logic_vector (0 to 3);
		clk_en, reset, ready: in bit;
		clk, clk_div8: in std_logic;				-- clk:2 MHz for output		clk_div8: 250 KHz for input
		i_chip, q_chip: out std_logic
		);
end sym_chip2;

architecture sym2chip_arch of sym_chip2 is
	type io_type is (input, output);
	type chips is array (0 to 15) of std_logic_vector(0 to 31);
	signal current_st: io_type:= input;
	signal in_sav: std_logic_vector(0 to 31):= (others => '0');
	constant seq: chips := (
									-- Chip Sequence			  -- Symbol Number
							"11011001110000110101001000101110",			-- 0
							"11101101100111000011010100100010",			-- 1
							"00101110110110011100001101010010",			-- 2
							"00100010111011011001110000110101",			-- 3
							"01010010001011101101100111000011",			-- 4
							"00110101001000101110110110011100",			-- 5
							"11000011010100100010111011011001",			-- 6
							"10011100001101010010001011101101",			-- 7
							"10111000110010010110000001110111",			-- 8
							"10111000110010010110000001110111",			-- 9
							"01111011100011001001011000000111",			-- 10
							"01110111101110001100100101100000",			-- 11
							"00000111011110111000110010010110",			-- 12
							"01100000011101111011100011001001",			-- 13
							"10010110000001110111101110001100",			-- 14
							"11001001011000000111011110111000"  		-- 15
							);
	begin
		process( clk, clk_div8, reset)
			variable cnt: integer range 0 to 31:= 0;
			variable full: bit:= '0';
			begin
				if reset= '0' then
					current_st<= input;
					cnt:= 0;
					i_chip<= 'Z';
					q_chip<= 'Z';
				elsif current_st= input then
					if falling_edge(clk_div8) then
						if clk_en= '0' then
							if ready= '1' then
								case (sym_in) is			-- sym_in: LSB --> MSB
									when "0000" => in_sav <= seq(0); full:= '1'; current_st<= output;
									when "1000" => in_sav <= seq(1); full:= '1'; current_st<= output;
									when "0100" => in_sav <= seq(2); full:= '1'; current_st<= output;
									when "1100" => in_sav <= seq(3); full:= '1'; current_st<= output;
									when "0010" => in_sav <= seq(4); full:= '1'; current_st<= output;
									when "1010" => in_sav <= seq(5); full:= '1'; current_st<= output;
									when "0110" => in_sav <= seq(6); full:= '1'; current_st<= output;
									when "1110" => in_sav <= seq(7); full:= '1'; current_st<= output;
									when "0001" => in_sav <= seq(8); full:= '1'; current_st<= output;
									when "1001" => in_sav <= seq(9); full:= '1'; current_st<= output;
									when "0101" => in_sav <= seq(10); full:= '1'; current_st<= output;
									when "1101" => in_sav <= seq(11); full:= '1'; current_st<= output;
									when "0011" => in_sav <= seq(12); full:= '1'; current_st<= output;
									when "1011" => in_sav <= seq(13); full:= '1'; current_st<= output;
									when "0111" => in_sav <= seq(14); full:= '1'; current_st<= output;
									when "1111" => in_sav <= seq(15); full:= '1'; current_st<= output;
									when others => in_sav <= (others => '0');
								end case;
							end if;
						end if;
					end if;
				elsif current_st= output then
					if falling_edge(clk) then
						if clk_en= '0' then
							if full= '1' then
								if cnt< 31 then
									if cnt mod 2= 0 then
										i_chip<= in_sav(0);
									else
										q_chip<= in_sav(0);
									end if;
									cnt:= cnt + 1;
									for i in 0 to 30 loop in_sav(i)<= in_sav(i+1); end loop;
									if cnt>= 31 then
										cnt:= 0;
										full:= '0';
										current_st<= input;
									end if;
								end if;
							end if;
						end if;
					end if;
				end if;
		end process;
end;