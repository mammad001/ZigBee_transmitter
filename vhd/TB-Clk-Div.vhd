-- Test Bench for symbol-to-chip module --
library ieee;
use ieee.std_logic_1164.all;

entity tb_clk_div is
end;

architecture tb_clk_div_arch of tb_clk_div is
  -- Signals --
	signal clk:  std_logic;
	signal reset, clk_en: bit;
	signal clk_div2, clk_div8, clk_div32: std_logic;
	-- Component --
	component clk_div
		port ( clk: in std_logic;
		reset, clk_en: in bit;
		clk_div2, clk_div8, clk_div32: out std_logic
		);
	end component;
	begin
cp_div: clk_div	port map (clk, reset, clk_en, clk_div2, clk_div8, clk_div32);
		reset<= '0',
			     	'1' after 6 us;
		clk_en<= '1',
	       			 '0' after 9 us;
	clock: 	process
				variable cp: std_logic := '1';
				begin
					cp:= not cp;
					clk<= cp;
					wait for 250 ns;		--Half a cycle for 2 MHz 	
			end process;
end;