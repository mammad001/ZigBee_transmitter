-- Test Bench for Bit to Symbol Module --
library ieee;
use ieee.std_logic_1164.all;

entity tb_bit_sym is
end;

architecture tb_bit_sym_arch of tb_bit_sym is
-- Signals --
	signal din: std_logic;
	signal clk: std_logic;
	signal reset, clk_en: bit;
	signal sym_out: std_logic_vector (0 to 3);
	signal ready: bit;
-- Component --
	component bit_symbol_2
		port(din: in std_logic;
		clk: in std_logic;
		clk_en, reset: in bit;
		sym_out: out std_logic_vector(0 to 3);
		ready: out bit);
	end component;
-- Body of Arch --
	begin
	m1: bit_symbol_2 port map (din, clk, clk_en, reset, sym_out, ready);
	reset<= '0',
			'1' after 6 us;
	clk_en<= '1',
			 '0' after 9 us;
	clock: process
				variable cp: std_logic := '1';
				begin
					cp:= not cp;
					clk<= cp;
					wait for 2 us;
		   end process;
	din<= '1',
		  '0' after 5 us,
		  '1' after 10 us,
		  '0' after 21 us,
		  '1' after 25 us;
end;